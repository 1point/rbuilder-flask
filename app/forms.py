from flask.ext.wtf import Form
from wtforms import TextField, BooleanField
from wtforms.validators import Required

class LoginForm(Form):
	e_mail = TextField('e_mail', validators = [Required()])
	remember_me = BooleanField('remember_me', default = False)
        password = TextField('password', validators = [Required()])

