from flask import render_template, flash, redirect
from app import app
from forms import LoginForm


@app.route('/')
@app.route('/index')
def index():
	return render_template('index.html',
	                       title = 'Home')


@app.route('/login', methods = ['GET', 'POST'])
def login():
	form = LoginForm()
	return render_template('login.html',
                               title = 'Sign In',
                               form = form)

@app.route('/notifier')
def notifier():
	return render_template('notifier.html',
                               title = 'Notifier')


@app.route('/hermiona')
def hermiona():
	return render_template('hermiona.html',
                               title = 'Hermiona')
